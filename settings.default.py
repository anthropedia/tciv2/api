import os


DEBUG = os.environ.get('DEBUG', False)
HOST = os.environ.get('HOST', 'localhost')
PORT = os.environ.get('PORT', 5000)

TCIAPI_TOKEN = os.environ.get('TCIAPI_TOKEN')

DATABASE = {'host': os.environ.get('MONGODB_URI', 'mongodb://localhost:12017/tci-api')}
