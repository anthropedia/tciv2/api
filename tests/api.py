import datetime
import json

from unittest import TestCase

from core import app
from core.models import AppToken, ScoreLog


def json_response(response):
    return json.loads(response.get_data().decode('utf-8'))


default_headers = {'Authentication': 'Bearer abc123',
                   'Content-Type': 'application/json'}


def get_data_fixtures():
    return {
        'answers': [3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1],
        'times': [1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5,1,1,3,4,3,3,4,4,4,1,5,1,3,4,4,4,1,5],
        'version': 'tcims',
        'country': 'fr'
    }


class BaseTest(TestCase):
    def setUp(self):
        AppToken(name='Test client', key='abc123').save()
        self.client = app.test_client()
        self.result = {
            'average_rawscores': {'co': 4, 'ha': 3.2, 'ns': 3.4, 'ps': 3.6, 'rd': 3.5, 'sd': 2.8, 'st': 3.2},
            'percentiles': {'co': 99, 'ha': 79, 'ns': 79, 'ps': 63, 'rd': 69, 'sd': 20, 'st': 55},
            'rawscores': {'co': 40, 'ha': 32, 'ns': 34, 'ps': 36, 'rd': 35, 'sd': 28, 'st': 32},
            'tscores': {'co': 73.5, 'ha': 58.2, 'ns': 58.2, 'ps': 53.1, 'rd': 54.8, 'sd': 41.3, 'st': 50.9},
            'zscores': {'co': 2.35, 'ha': 0.82, 'ns': 0.82, 'ps': 0.31, 'rd': 0.48, 'sd': -0.87, 'st': 0.09},
            'validity': 1,
        }

    def tearDown(self):
        AppToken.drop_collection()
        ScoreLog.drop_collection()


class APIStableTest(BaseTest):
    def test_ping(self):
        response = self.client.get('/ping/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_data(), b'pong')

    def test_questions_return_240_items(self):
        response = self.client.get('/questions/tci3240/',
                                   headers=default_headers)
        self.assertEqual(response.status_code, 200)
        body = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(body), 240)

    def test_score_return_expected_keys(self):
        response = self.client.post('/score/',
                                    data=json.dumps(get_data_fixtures()),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        body = json.loads(response.get_data(as_text=True))
        self.assertIn('average_rawscores', body.keys())


class AppTokenModelTest(BaseTest):

    def test_generates_key(self):
        token = AppToken(name='Test app').save()
        self.assertEqual(len(token.key), 32)

    def test_saves_creation_date(self):
        token = AppToken(name='Test app').save()
        self.assertEqual(token.creation_date.ctime(),
                         datetime.datetime.utcnow().ctime())


class APIV1AuthTest(BaseTest):

    def test_ping(self):
        response = self.client.get('/v1/ping/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_data(), b'pong')

    def test_no_authentication_raises_401(self):
        response = self.client.get('/v1/questions/tcims/')
        self.assertEqual(response.status_code, 401)

    def test_authenticate_wrong_token_raises_401(self):
        response = self.client.get('/v1/questions/tcims/',
                                   headers={'Authentication': 'Bearer wrong'})
        self.assertEqual(response.status_code, 401)

    def test_authentication_token_grants_access(self):
        response = self.client.get('/v1/questions/tcims/',
                                   headers=default_headers)
        self.assertEqual(response.status_code, 200)

    def test_authenticate_malformatted_token_raises_400(self):
        response = self.client.get('/v1/questions/tcims/',
                                   headers={'Authentication': 'Bearer a d'})
        self.assertEqual(response.status_code, 400)


class APIV1QuestionsTest(BaseTest):

    def test_get_questions_return_list_of_questions(self):
        response = self.client.get('/v1/questions/tcims/',
                                   headers=default_headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        self.assertEqual(len(json_response(response)), 72)

    def test_get_questions_with_no_version_raises_404(self):
        response = self.client.get('/v1/questions/')
        self.assertEqual(response.status_code, 404)

    def test_get_questions_unknown_version_returns_error(self):
        response = self.client.get('/v1/questions/unknown/',
                                   headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual('No TCI version "unknown" exists.',
                         json_response(response)['error'])


class APIV1ScoreTest(BaseTest):

    def test_score_post_returns_all_properties(self):
        response = self.client.post('/v1/score/',
                                    data=json.dumps(get_data_fixtures()),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        self.assertEqual(self.result.keys(), json_response(response).keys())

    def test_unknown_country_defaults_to_international(self):
        data = get_data_fixtures()
        data['country'] = 'zz'
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        self.assertEqual(self.result.keys(), json_response(response).keys())

    def test_score_returns_expected_score_values(self):
        response = self.client.post('/v1/score/',
                                    data=json.dumps(get_data_fixtures()),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        self.assertEqual(json_response(response), self.result)

    def test_wrong_version_returns_error(self):
        data = get_data_fixtures()
        data['version'] = 'tci_wrong'
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        result = json_response(response)
        self.assertEqual(result.get('error'),
                         'No TCI version "tci_wrong" exists.')

    def test_smaller_number_of_answers_returns_error(self):
        data = get_data_fixtures()
        data['answers'] = [3, 4, 2]
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        result = json_response(response)
        self.assertEqual(result.get('error'),
                         'A TCI "tcims" requires 72 answers. 3 were given.')

    def test_larger_number_of_answers_and_times_returns_error(self):
        data = get_data_fixtures()
        data['answers'] += [1]
        data['times'] += [1000]
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        result = json_response(response)
        self.assertEqual(result.get('error'),
                         'A TCI "tcims" requires 72 answers. 73 were given.')

    def test_smaller_number_of_times_returns_error(self):
        data = get_data_fixtures()
        data['times'] = [3, 4, 2]
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        result = json_response(response)
        self.assertEqual(result.get('error'),
                         '3 times were sent. 0 or 72 were expected.')

    def test_smaller_number_of_times_and_answers_returns_error(self):
        data = get_data_fixtures()
        data['times'] = [3, 4, 2]
        data['answers'] = [2, 5, 1]
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers.get('Content-Type'),
                         'application/json')
        result = json_response(response)
        self.assertEqual(result.get('error'),
                         'A TCI "tcims" requires 72 answers. 3 were given.')

    def test_score_sending_times_is_not_required(self):
        data = get_data_fixtures()
        del data['times']
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)

    def test_data_with_an_answer_greater_than_5_raises_400(self):
        data = get_data_fixtures()
        data['answers'][0] = 6
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)

    def test_data_with_an_answer_smaller_than_1_raises_400(self):
        data = get_data_fixtures()
        data['answers'][0] = 0
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 400)

    def test_data_with_an_answer_float_value_get_fixed(self):
        data = get_data_fixtures()
        data['answers'][0] = 3.5
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)


class APIV1ScoreLogTest(BaseTest):

    def setUp(self):
        super().setUp()
        self.score_logs = [self.create_score(1, 100),
                           self.create_score(1, 101)]
        self.score_logs.extend([self.create_score(i + 1) for i in range(5)])

    def create_score(self, i, rand=None):
        return ScoreLog.objects.create(
            version='tci3240',
            answers=[1, 2, 3, rand or i],
            creation_date='2018-0{i}-0{i}'.format(i=i),
            token=AppToken.objects.get(key='abc123'),
        )

    def test_score_logs_infos(self):
        data = get_data_fixtures()
        data['reference'] = 'user1'
        self.assertEqual(ScoreLog.objects.count(), 0)
        response = self.client.post('/v1/score/',
                                    data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ScoreLog.objects.count(), 1)

        app_log = ScoreLog.objects.first()
        self.assertEqual(len(app_log.answers), 72)
        self.assertEqual(len(app_log.times), 72)
        self.assertEqual(app_log.reference, 'user1')
        self.assertEqual(app_log.token.key, 'abc123')
        self.assertEqual(app_log.creation_date.ctime(),
                         datetime.datetime.utcnow().ctime())

    def test_score_logs_only_once_same_answers(self):
        data = get_data_fixtures()
        self.assertEqual(ScoreLog.objects.count(), 0)
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 201)
        data['reference'] = 'changedRefThatShouldntMatter'
        response = self.client.post('/v1/score/', data=json.dumps(data),
                                    headers=default_headers)
        self.assertEqual(response.status_code, 200)
        data['answers'][0] = 5
        self.client.post('/v1/score/', data=json.dumps(data),
                         headers=default_headers)
        self.assertEqual(ScoreLog.objects.count(), 2)

    def test_score_logs_access_return_json_list(self):
        response = self.client.get('/v1/logs/', headers=default_headers)
        self.assertEqual(response.status_code, 200)
        result = json_response(response)
        self.assertEqual(len(result), 7)
        self.assertEqual(result[0], self.score_logs[6])

    def test_score_logs_meta_access(self):
        response = self.client.get('/v1/logs/meta/', headers=default_headers)
        self.assertEqual(response.status_code, 200)
        result = json_response(response)
        self.assertEqual(set(result.keys()), set(['total', 'current_month',
                                                  'last_month']))
        self.assertEqual(result['total'], 7)
