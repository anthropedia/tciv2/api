import datetime
from collections import OrderedDict

from flask import Blueprint, jsonify, abort, request, make_response, g
from tcicore.tci import TCI, TCIValidity
import tcidata

from . import app
from .models import AppToken, ScoreLog
from .utils import json_abort, auth_required


@auth_required
def questions(version):
    try:
        result = tcidata.get_tci(version)
    except NotImplementedError as error:
        json_abort(400, str(error))
    return jsonify(result['questions'])


@auth_required
def score():
    response = request.get_json() or {}
    try:
        values = {
            'version': str(response['version']),
            'country': response.get('country', ''),
            'answers': list(response['answers']),
            'times': list(response.get('times', [])),
        }
    except KeyError as error:
        json_abort(400, f'{error.args[0]} is missing.')
    try:
        tci = TCI(version=values['version'],
                  answers=values['answers'],
                  country=values['country'])
        tci_validity = TCIValidity(version=values['version'],
                                   answers=values['answers'],
                                   times=values['times'])
    except (NotImplementedError, ValueError) as error:
        json_abort(400, str(error))
    try:
        data = {
            'validity': int(tci_validity.items_score),
            'percentiles': dict(tci.percentiles),
            'tscores': dict(tci.tscores),
            'zscores': dict(tci.zscores),
            'average_rawscores': dict(tci.average_rawscores),
            'rawscores': OrderedDict(tci.rawscores),
        }
    except IndexError as error:
        json_abort(400, str(error))

    status_code = 200
    score = ScoreLog(token=g.app_token, **response)

    if not ScoreLog.objects(uid=score.unique_id).count():
        score.save()
        status_code = 201

    return jsonify(data), status_code


@auth_required
def logs():
    scores = ScoreLog.objects(token=g.app_token)
    return jsonify([s.to_dict() for s in scores])


@auth_required
def logs_meta():
    scores = ScoreLog.objects(token=g.app_token)
    today = datetime.datetime.today()
    current_month = today.replace(day=1)
    last_month = (current_month - datetime.timedelta(days=1)).replace(day=1)
    current_logs = scores.filter(creation_date__gte=current_month)
    last_logs = scores.filter(creation_date__gte=last_month,
                              creation_date__lt=current_month)
    data = {
        'current_month': len(current_logs),
        'last_month': len(last_logs),
        'total': len(scores),
    }
    return jsonify(data)
