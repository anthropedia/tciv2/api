from functools import wraps

from flask import make_response, jsonify, abort, request, g

from .models import AppToken


def json_abort(status_code, message):
    abort(make_response(jsonify(error=message), status_code))


def validate_token():
    if not request.headers.get('Authentication', '').startswith('Bearer '):
        json_abort(401, 'The app  did not provide an authentication token.')
    try:
        _, token = request.headers['Authentication'].split(' ')
    except ValueError:
        json_abort(400, 'The app authentication header is invalid.')
    try:
        g.app_token = AppToken.objects.get(key=token)
        return token
    except AppToken.DoesNotExist:
        json_abort(401, 'The app authentication token is invalid.')


def auth_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if validate_token():
            return func(*args, **kwargs)
    return wrapper


def admin_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        token = validate_token()
        if token.is_super_admin:
            return func(*args, **kwargs)
    return wrapper
