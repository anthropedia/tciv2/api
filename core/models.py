import datetime
from uuid import uuid4
from hashlib import md5

from . import db


class AppToken(db.Document):
    name = db.StringField(required=True, unique=True)
    key = db.StringField(required=True, unique=True)
    creation_date = db.DateTimeField(default=datetime.datetime.utcnow)

    def generate_key(self):
        return str(uuid4()).replace('-', '')

    def is_super_admin(self):
        return self.name == 'Anthropedia'

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)


class ScoreLog(db.Document):
    reference = db.StringField()
    version = db.StringField(required=True)
    country = db.StringField(min_length=2, max_length=2)
    answers = db.ListField(required=True)
    times = db.ListField()
    creation_date = db.DateTimeField(default=datetime.datetime.utcnow)
    token = db.ReferenceField(AppToken, required=True)
    uid = db.StringField(required=True, unique=True, min_length=32,
                         max_length=32)

    meta = {
        'ordering': ['-creation_date']
    }

    @property
    def unique_id(self):
        return md5(str(self.answers).encode()).hexdigest()

    def __str__(self):
        return (f'{self.version} for {self.reference or "anonymous"} '
                f'on {self.creation_date}')

    def __eq__(self, uid):
        if isinstance(uid, ScoreLog):
            uid = uid.uid
        elif isinstance(uid, dict):
            uid = uid.get('uid')
        return self.uid == uid

    def to_dict(self):
        return {
            'id': str(self.id),
            'version': self.version,
            'answers': self.answers,
            'times': self.times,
            'creation_date': str(self.creation_date),
            'uid': self.uid,
            'token': self.token.key,
        }

    def save(self, *args, **kwargs):
        self.uid = self.unique_id
        return super().save(*args, **kwargs)
