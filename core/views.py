from flask import jsonify

from . import app
from .v1 import questions, score, logs, logs_meta
from .models import ScoreLog
from core.utils import admin_required


def ping():
    return 'pong'


app.add_url_rule('/ping/', 'ping', view_func=ping)
app.add_url_rule('/questions/<version>/', 'questions', view_func=questions)
app.add_url_rule('/score/', 'score', view_func=score, methods=['post'])

app.add_url_rule('/v1/ping/', 'ping', view_func=ping)
app.add_url_rule('/v1/questions/<version>/', 'questions', view_func=questions)
app.add_url_rule('/v1/score/', 'score', view_func=score, methods=['post'])
app.add_url_rule('/v1/logs/', 'logs', view_func=logs)
app.add_url_rule('/v1/logs/meta/', 'logs_meta', view_func=logs_meta)
