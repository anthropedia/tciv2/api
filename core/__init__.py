import sys
from pathlib import Path

from flask import Flask
import mongoengine as db
from raven.contrib.flask import Sentry

settings_file = 'settings.py'
if 'unittest' in sys.argv[0]:
    settings_file = 'settings.test.py'

PROJECT_PATH = Path(__file__).parent.parent

app = Flask('tci-api')
app.config.from_pyfile(PROJECT_PATH / 'settings.default.py')
app.config.from_pyfile(PROJECT_PATH / settings_file, silent=True)

db.connect(**app.config['DATABASE'])

if not app.debug:
    sentry = Sentry(app, dsn='https://781f1c05ac944728810a648bc980797b:95fce5dbe7c14398bd5843ab229d3c28@sentry.io/271268')

from . import v1, views  # noqa: F401
