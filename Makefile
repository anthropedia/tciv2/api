git_update=git fetch origin master && git reset --hard FETCH_HEAD

help:
	return "Make tasks for deployment. Checkout the makefile content."

generate-token:  # name=<string>
	@echo "Generated token:"
	@python -c "from core.models import AppToken; token=AppToken.objects.create(name='${name}'); print(token.key)"

deploy:
	ssh epidaurus "cd ~/tci-api && ${git_update}"
	ssh epidaurus "cd ~/tci-compose && docker-compose restart api"

logs:
	ssh epidaurus "cd ~/tci-compose && docker-compose logs -f api"
