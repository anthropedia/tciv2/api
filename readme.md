## Requirements

- Python 3.5+
- [MongoDB](https://www.mongodb.com/) or [Docker Compose](https://docs.docker.com/compose/)
- Git
- A private access to the Anthropedia's Bitbucket.


## Installation

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    cp settings{.example.py,.py}

You may now customize your settings editing _settings.py_.

This project will auto-install [tci-core](https://bitbucket.org/anthropedia/tci-core)
and [tci-data](https://bitbucket.org/anthropedia/tci-data)
as dependencies.


## Running the project

    docker-compose up -d
    python app.py

You may optionally set the _SETTINGS_ environment variable to point to your
_settings.py_ file:

    SETTINGS=`pwd`/settings.py python app.py


## Authentication

When getting response with a status code 401, it means you most probably
don't pass a valid token key to the API.

Calling endpoints require authentication.

1. Generate a token

```
$ python
> from core.models import AppToken
AppToken.objects.create(name="my app", token="abc123")
```

2. Connect to the API

```
http localhost:5000/questions/tci3240/ 'Authentication:Bearer abc123'
```

> The example here is using HTTPie, but cURL, Postman, or whatever you use to
make API calls should work.

Where `abc123` is the key you created in step 1.

This will return a list of 240 questions.


## Endpoints

> Calling endpoints requires authentication from AppToken in the DB.
> You must pass `Authentication` header with a `Bearer` as following.
> Example: `requests('/questions/tcims/', headers={'Authentication': 'Bearer m45up3rt0k3n'})`


#### GET /questions/<version>/

List of questions for a test version.


#### POST /score/

```
{
  'version': <str>,
  'answers': <list>,
  'times': <list (optional)>,
  'country': <str{2}>,
  'reference': <str (optional)>
}
```
